from PyQt5 import QtCore, QtGui, QtWidgets, uic
from PyQt5.QtCore import *
from PyQt5.QtGui import *


class list_of_reservation(QtWidgets.QWidget):
    def __init__(self):
        super(list_of_reservation, self).__init__()  # Call the inherited classes __init__ method
        uic.loadUi("list_of_reservation.ui", self)

        self.data_reservation = self.findChild(QtWidgets.QLabel, "data_reservation")

        self.data_reservation_change = self.findChild(QtWidgets.QLabel, "data_reservation_change")

        self.type_of_table = self.findChild(QtWidgets.QLabel, "type_of_table")

        self.type_of_table_change = self.findChild(QtWidgets.QLabel, "type_of_table_change")

        self.table_number = self.findChild(QtWidgets.QLabel, "table_number")

        self.table_number_change = self.findChild(QtWidgets.QLabel, "table_number_change")

        self.user = self.findChild(QtWidgets.QLabel, "user")

        self.user_change = self.findChild(QtWidgets.QLabel, "user_change")

        self.background_list_of_reservation = self.findChild(QtWidgets.QLabel, "background_list_of_reservation")
        self.background_list_of_reservation.setStyleSheet("background-color: lightgreen")

        self.communique_to_reservation = self.findChild(QtWidgets.QLabel, "communique_to_reservation")

        self.confirm_reservation = self.findChild(QtWidgets.QPushButton, "confirm_reservation")
        self.confirm_reservation.clicked.connect(self.to_confirm_reservation)

        self.cancel_reservation = self.findChild(QtWidgets.QPushButton, "cancel_reservation")
        self.cancel_reservation.clicked.connect(self.to_cancel_reservation)

        # self.show()

    def to_confirm_reservation(self):
        self.confirm_reservation.hide()
        self.cancel_reservation.hide()
        self.communique_to_reservation.setText("Rezerwacja potwierdzona")
        self.communique_to_reservation.show()

    def to_cancel_reservation(self):
        self.confirm_reservation.hide()
        self.cancel_reservation.hide()
        self.communique_to_reservation.setText("Rezerwacja odrzucona")
        self.communique_to_reservation.show()

# if __name__ == "__main__":
#     import sys
#     app = QtWidgets.QApplication(sys.argv)
#     new = list_of_reservation()
#     sys.exit(app.exec_())