CREATE DATABASE restaurant;
USE restaurant;

-- 1
CREATE TABLE person (
  id_person int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name varchar(45) NOT NULL,
  surname varchar(60) NOT NULL,
  street varchar(60) DEFAULT NULL,
  street_number varchar(10) DEFAULT NULL,
  apartment_number varchar(4) DEFAULT NULL
);

-- 2
CREATE TABLE menu (
  id_menu INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  meal_name VARCHAR(100) NOT NULL,
  ingredients VARCHAR(200) NULL,
  price DECIMAL(5,2) NOT NULL,
  meal_type INT NOT NULL,
  availability TINYINT(1) NOT NULL
  );

-- 3
CREATE TABLE table_in_the_restaurant (
  id_table INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  table_number DECIMAL(3,0) NOT NULL,
  number_of_people_at_the_table DECIMAL(2,0) NOT NULL
  );

-- 4
CREATE TABLE order_from_restaurant (
  id_order INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  id_client INT NOT NULL,
  order_number INT NOT NULL,
  total_price DECIMAL(6,2) NOT NULL,
  id_courier INT NOT NULL
  );

-- 5
CREATE TABLE ordered_meals (
  id_ordered_meals INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  id_meal INT NOT NULL,
  quantity DECIMAL(3,0) NOT NULL,
  order_number INT NOT NULL
  );
  
  -- 6
  CREATE TABLE role (
  id_role INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  role_name VARCHAR(30) NOT NULL
  );
  
  -- 7 
  CREATE TABLE giving_role (
  id_giving_role INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  id_person INT NOT NULL,
  id_role INT NOT NULL
  );

-- 8
CREATE TABLE type_of_meal (
  id_type_of_meal INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  meal_name VARCHAR(100) NOT NULL
  );
  
  -- 9
  CREATE TABLE table_reservation (
  id_table_reservation INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  id_table INT NOT NULL,
  id_client INT NOT NULL,
  data_reservation DATE NOT NULL,
  hour_reservation TIME NOT NULL
  );
  
  -- 10
  
