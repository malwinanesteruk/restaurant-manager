from PyQt5 import QtCore, QtGui, QtWidgets, uic
from PyQt5.QtCore import *
from PyQt5.QtGui import *


class list_of_generated_code(QtWidgets.QWidget):
    def __init__(self):
        super(list_of_generated_code, self).__init__()  # Call the inherited classes __init__ method
        uic.loadUi("list_of_generated_code.ui", self)

        self.background_list_of_generated_code = self.findChild(QtWidgets.QLabel, "background_list_of_generated_code")
        self.background_list_of_generated_code.setStyleSheet("background-color: lightgreen")

        self.code_name = self.findChild(QtWidgets.QLabel, "code_name")

        self.code_name_2 = self.findChild(QtWidgets.QLabel, "code_name_2")

        self.result = self.findChild(QtWidgets.QLabel, "result")

        self.result_2 = self.findChild(QtWidgets.QLabel, "result_2")

        self.uptime = self.findChild(QtWidgets.QLabel, "uptime")

        self.uptime_2 = self.findChild(QtWidgets.QLabel, "uptime_2")

        self.status = self.findChild(QtWidgets.QLabel, "status")
        self.status.setFont(QFont('Arial', 10, weight=QFont.Bold))

        self.delete_code = self.findChild(QtWidgets.QPushButton, "delete_code")
        self.delete_code.clicked.connect(self.delete_code_method)

        # self.show()

    def delete_code_method(self):
        self.status.setText("USUNIĘTY")
        self.status.setFont(QFont('Arial', 10, weight=QFont.Bold))
        self.status.move(448,230)
        self.status.show()
        self.delete_code.hide()

# if __name__ == "__main__":
#     import sys
#     app = QtWidgets.QApplication(sys.argv)
#     new = list_of_generated_code()
#     sys.exit(app.exec_())