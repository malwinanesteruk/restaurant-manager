from PyQt5 import QtCore, QtGui, QtWidgets, uic
from PyQt5.QtCore import *
from PyQt5.QtGui import *

class basket_order_full(QtWidgets.QWidget):
    def __init__(self, name, price):
        super(basket_order_full, self).__init__()  # Call the inherited classes __init__ method
        uic.loadUi("basket_order_full.ui", self)  # Load the .ui file

        self.name_meal = self.findChild(QtWidgets.QLabel, "name_meal")
        self.name_meal.setText(name)
        self.name_meal.show()

        self.price_meal = self.findChild(QtWidgets.QLabel, "price_meal")
        self.price_meal.setText(price)
        self.price_meal.show()

        self.quantity_meal = self.findChild(QtWidgets.QLabel, "quantity_meal")

        self.minus_button = self.findChild(QtWidgets.QPushButton, "minus_button")
        self.minus_button.setFont(QFont('Arial', 20, weight=QFont.Bold))
        self.minus_button.clicked.connect(self.minus_meal)

        self.plus_button = self.findChild(QtWidgets.QPushButton, "plus_button")
        self.plus_button.setFont(QFont('Arial', 20, weight=QFont.Bold))
        self.plus_button.clicked.connect(self.plus_meal)

        self.background_busket = self.findChild(QtWidgets.QLabel, "background_busket")
        self.background_busket.setStyleSheet("background-color: lightgreen")

        self.counter = 1

    def plus_meal(self):
        self.counter += 1
        self.quantity = str(self.counter)
        self.quantity_meal.setText(self.quantity + " szt")


    def minus_meal(self):
        if self.counter == 1:
            #self.parent().parent().parent().parent().parent().delete_meal_from_left_scroll_area()
            #self.close()
            print("nicz")
        else:
            self.counter -= 1
            self.quantity = str(self.counter)
            self.quantity_meal.setText(self.quantity + " szt")

#         self.show()
#
# if __name__ == "__main__":
#     import sys
#     app = QtWidgets.QApplication(sys.argv)
#     new = basket_order_full()
#     sys.exit(app.exec_())