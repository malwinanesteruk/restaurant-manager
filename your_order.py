from PyQt5 import QtCore, QtGui, QtWidgets, uic

class your_order(QtWidgets.QWidget):
    def __init__(self):
        super(your_order, self).__init__()  # Call the inherited classes __init__ method
        uic.loadUi("your_order.ui", self)  # Load the .ui file

        self.background_your_order = self.findChild(QtWidgets.QLabel, "background_your_order")
        self.background_your_order.setStyleSheet("background-color: lightgreen")

        self.order_number_label = self.findChild(QtWidgets.QLabel, "order_number_label")

        self.order_number_label_change = self.findChild(QtWidgets.QLabel, "order_number_label_change")

        self.status_label = self.findChild(QtWidgets.QLabel, "status_label")

        self.status_label_change = self.findChild(QtWidgets.QLabel, "status_label_change")

        self.date_of_order_label = self.findChild(QtWidgets.QLabel, "date_of_order_label")

        self.date_of_order_label_change = self.findChild(QtWidgets.QLabel, "date_of_order_label_change")

        self.order_value_label = self.findChild(QtWidgets.QLabel, "order_value_label")

        self.order_value_label_change = self.findChild(QtWidgets.QLabel, "order_value_label_change")

        self.payment_method_label = self.findChild(QtWidgets.QLabel, "payment_method_label")

        self.payment_method_label_change = self.findChild(QtWidgets.QLabel, "payment_method_label_change")

        self.details_of_the_order_button = self.findChild(QtWidgets.QPushButton, "details_of_the_order_button")


#         self.show()
#
# if __name__ == "__main__":
#     import sys
#     app = QtWidgets.QApplication(sys.argv)
#     new = your_order()
#     sys.exit(app.exec_())
