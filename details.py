from PyQt5 import QtCore, QtGui, QtWidgets, uic
from PyQt5.QtCore import *
from PyQt5.QtGui import *


class details(QtWidgets.QWidget):
    def __init__(self):
        super(details, self).__init__()  # Call the inherited classes __init__ method
        uic.loadUi("details.ui", self)

        self.name = self.findChild(QtWidgets.QLabel, "name")

        self.name_label = self.findChild(QtWidgets.QLabel, "name_label")

        self.surname = self.findChild(QtWidgets.QLabel, "surname")

        self.surname_label = self.findChild(QtWidgets.QLabel, "surname_label")

        self.adress = self.findChild(QtWidgets.QLabel, "adress")

        self.adress_label = self.findChild(QtWidgets.QLabel, "adress_label")

        self.background_details = self.findChild(QtWidgets.QLabel, "background_details")
        self.background_details.setStyleSheet("background-color: lightgreen")

#         self.show()
#
#
#
#
# if __name__ == "__main__":
#     import sys
#     app = QtWidgets.QApplication(sys.argv)
#     new = details()
#     sys.exit(app.exec_())