from PyQt5 import QtCore, QtGui, QtWidgets, uic
from PyQt5.QtCore import *
from PyQt5.QtGui import *


class end_generate_code(QtWidgets.QWidget):
    def __init__(self):
        super(end_generate_code, self).__init__()  # Call the inherited classes __init__ method
        uic.loadUi("end_generate_code.ui", self)

        self.communique_end_generate_coupon = self.findChild(QtWidgets.QLabel, "communique_end_generate_coupon")

        self.background_communique_end_generate_coupon = self.findChild(QtWidgets.QLabel, "background_communique_end_generate_coupon")
        self.background_communique_end_generate_coupon.setStyleSheet("background-color: lightgreen")

        self.close_end_table_reservation = self.findChild(QtWidgets.QPushButton, "close_end_table_reservation")
        self.close_end_table_reservation.setIcon(QtGui.QIcon('close_icon.png'))
        self.close_end_table_reservation.setIconSize(QtCore.QSize(25, 25))
        self.close_end_table_reservation.clicked.connect(self.close_to_end_generate_code)

        self.show()

    def close_to_end_generate_code(self):
        self.communique_end_generate_coupon.hide()
        self.background_communique_end_generate_coupon.hide()
        self.close_end_table_reservation.hide()
        self.parent().parent().parent().closed_end_generate_code()

# if __name__ == "__main__":
#     import sys
#     app = QtWidgets.QApplication(sys.argv)
#     new = end_generate_code()
#     sys.exit(app.exec_())