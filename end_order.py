from PyQt5 import QtCore, QtGui, QtWidgets, uic


class end_order(QtWidgets.QWidget):
    def __init__(self):
        super(end_order, self).__init__()  # Call the inherited classes __init__ method
        uic.loadUi("end_order.ui", self)  # Load the .ui file

        self.communique_end_order = self.findChild(QtWidgets.QLabel, "communique_end_order")

        self.background_communique_end_order = self.findChild(QtWidgets.QLabel, "background_communique_end_order")
        self.background_communique_end_order.setStyleSheet("background-color: lightgreen")

        self.close_end_order = self.findChild(QtWidgets.QPushButton, "close_end_order")
        self.close_end_order.setIcon(QtGui.QIcon('close_icon.png'))
        self.close_end_order.setIconSize(QtCore.QSize(25, 25))
        self.close_end_order.clicked.connect(self.to_close_end_order)

    def to_close_end_order(self):
        self.communique_end_order.hide()
        self.background_communique_end_order.hide()
        self.close_end_order.hide()
        self.parent().parent().parent().closed_end_order()

#         self.show()
#
# if __name__ == "__main__":
#     import sys
#     app = QtWidgets.QApplication(sys.argv)
#     new = end_order()
#     sys.exit(app.exec_())