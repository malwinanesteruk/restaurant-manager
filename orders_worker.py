from PyQt5 import QtCore, QtGui, QtWidgets, uic
from PyQt5.QtCore import *
from PyQt5.QtGui import *


class orders_worker(QtWidgets.QWidget):
    def __init__(self):
        super(orders_worker, self).__init__()  # Call the inherited classes __init__ method
        uic.loadUi("orders_worker.ui", self)

        self.order_number_label = self.findChild(QtWidgets.QLabel, "order_number_label")

        self.order_number_label_change = self.findChild(QtWidgets.QLabel, "order_number_label_change")

        self.background_orders_worker = self.findChild(QtWidgets.QLabel, "background_orders_worker")
        self.background_orders_worker.setStyleSheet("background-color: lightgreen")

        self.status_1 = self.findChild(QtWidgets.QPushButton, "status_1")
        self.status_1.clicked.connect(self.to_show_status_1)

        self.status_2 = self.findChild(QtWidgets.QPushButton, "status_2")
        self.status_2.clicked.connect(self.to_show_status_2)

        self.status_3 = self.findChild(QtWidgets.QPushButton, "status_3")
        self.status_3.clicked.connect(self.to_show_status_3)
        self.status_3.setEnabled(False)

        self.status_4 = self.findChild(QtWidgets.QPushButton, "status_4")
        self.status_4.clicked.connect(self.to_show_status_4)
        self.status_4.setEnabled(False)

        self.status_5 = self.findChild(QtWidgets.QPushButton, "status_5")
        self.status_5.clicked.connect(self.to_show_status_5)
        self.status_5.setEnabled(False)

        self.details_of_the_order_button = self.findChild(QtWidgets.QPushButton, "details_of_the_order_button")

        # self.show()

    def to_show_status_1(self):
        self.status_1.setEnabled(False)
        self.status_1.setStyleSheet("background-color: lightgreen")
        self.status_2.setEnabled(False)
        self.status_3.setEnabled(False)
        self.status_4.setEnabled(False)
        self.status_5.setEnabled(False)

    def to_show_status_2(self):
        self.status_1.setEnabled(False)
        self.status_2.setEnabled(False)
        self.status_2.setStyleSheet("background-color: lightgreen")
        self.status_3.setEnabled(True)
        self.status_4.setEnabled(False)
        self.status_5.setEnabled(False)

    def to_show_status_3(self):
        self.status_1.setEnabled(False)
        self.status_2.setEnabled(False)
        self.status_3.setEnabled(False)
        self.status_3.setStyleSheet("background-color: lightgreen")
        self.status_4.setEnabled(True)
        self.status_5.setEnabled(False)

    def to_show_status_4(self):
        self.status_1.setEnabled(False)
        self.status_2.setEnabled(False)
        self.status_3.setEnabled(False)
        self.status_4.setEnabled(False)
        self.status_4.setStyleSheet("background-color: lightgreen")
        self.status_5.setEnabled(True)

    def to_show_status_5(self):
        self.status_1.setEnabled(False)
        self.status_2.setEnabled(False)
        self.status_3.setEnabled(False)
        self.status_4.setEnabled(False)
        self.status_5.setEnabled(False)
        self.status_5.setStyleSheet("background-color: lightgreen")



# if __name__ == "__main__":
#     import sys
#     app = QtWidgets.QApplication(sys.argv)
#     new = orders_worker()
#     sys.exit(app.exec_())