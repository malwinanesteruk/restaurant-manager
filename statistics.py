from PyQt5 import QtCore, QtGui, QtWidgets, uic
from PyQt5.QtCore import *
from PyQt5.QtGui import *


class statistics(QtWidgets.QWidget):
    def __init__(self):
        super(statistics, self).__init__()  # Call the inherited classes __init__ method
        uic.loadUi("statistics.ui", self)

        self.communique_label_1 = self.findChild(QtWidgets.QLabel, "communique_label_1")

        self.daily_button = self.findChild(QtWidgets.QPushButton, "daily_button")

        self.in_total_button = self.findChild(QtWidgets.QPushButton, "in_total_button")

        self.communique_label_2 = self.findChild(QtWidgets.QLabel, "communique_label_2")

        self.calendar_statistics = self.findChild(QtWidgets.QLabel, "calendar_statistics")

        self.communique_label_3 = self.findChild(QtWidgets.QLabel, "communique_label_3")

        self.usually_button = self.findChild(QtWidgets.QPushButton, "usually_button")

        self.category_button = self.findChild(QtWidgets.QPushButton, "category_button")

        self.best_sold_button = self.findChild(QtWidgets.QPushButton, "best_sold_button")

        self.graph = self.findChild(QtWidgets.QLabel, "graph")

        self.close_graph_button = self.findChild(QtWidgets.QPushButton, "close_graph_button")

        self.background_statistics = self.findChild(QtWidgets.QLabel, "background_statistics")
        self.background_statistics.setStyleSheet("background-color: lightgreen")

        self.show()


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    new = statistics()
    sys.exit(app.exec_())