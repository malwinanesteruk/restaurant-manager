from PyQt5 import QtCore, QtGui, QtWidgets, uic
from PyQt5.QtCore import *
from PyQt5.QtGui import *


class home_page(QtWidgets.QWidget):
    def __init__(self):
        super(home_page, self).__init__()  # Call the inherited classes __init__ method
        uic.loadUi("home_page.ui", self)

        self.name_of_restaurant = self.findChild(QtWidgets.QLabel, "name_of_restaurant")
        self.name_of_restaurant.setStyleSheet("background-color: lightgreen")

        self.picture = self.findChild(QtWidgets.QLabel, "picture")

        self.text_1 = self.findChild(QtWidgets.QLabel, "text_1")

        self.text_2 = self.findChild(QtWidgets.QLabel, "text_2")
        self.text_2.setStyleSheet("background-color: lightgreen")

        self.text_3 = self.findChild(QtWidgets.QLabel, "text_3")

        self.text_4 = self.findChild(QtWidgets.QLabel, "text_4")
        self.text_4.setStyleSheet("background-color: lightgreen")

        self.text_change = self.findChild(QtWidgets.QLabel, "text_change")

        self.switch_button = self.findChild(QtWidgets.QPushButton, "switch_button")
        self.switch_button.hide()
#
#         self.show()
#
#
# if __name__ == "__main__":
#     import sys
#     app = QtWidgets.QApplication(sys.argv)
#     new = home_page()
#     sys.exit(app.exec_())