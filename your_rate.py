from PyQt5 import QtCore, QtGui, QtWidgets, uic

class your_rate(QtWidgets.QWidget):
    def __init__(self):
        super(your_rate, self).__init__()  # Call the inherited classes __init__ method
        uic.loadUi("your_rate.ui", self)  # Load the .ui file

        self.meal_name = self.findChild(QtWidgets.QLabel, "meal_name")

        self.meal_name_change = self.findChild(QtWidgets.QLabel, "meal_name_change")

        self.rate = self.findChild(QtWidgets.QLabel, "rate")

        self.rate_change = self.findChild(QtWidgets.QLabel, "rate_change")

        self.comment = self.findChild(QtWidgets.QLabel, "comment")

        self.comment_change = self.findChild(QtWidgets.QLabel, "comment_change")

        self.your_rate_background = self.findChild(QtWidgets.QLabel, "your_rate_background")
        self.your_rate_background.setStyleSheet("background-color: lightgreen")

#         self.show()
#
# if __name__ == "__main__":
#     import sys
#     app = QtWidgets.QApplication(sys.argv)
#     new = your_rate()
#     sys.exit(app.exec_())