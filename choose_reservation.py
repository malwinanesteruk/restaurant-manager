from PyQt5 import QtCore, QtGui, QtWidgets, uic
from PyQt5.QtCore import *
from PyQt5.QtGui import *


class choose_reservation(QtWidgets.QWidget):
    def __init__(self):
        super(choose_reservation, self).__init__()  # Call the inherited classes __init__ method
        uic.loadUi("choose_reservation.ui", self)  # Load the .ui file

        self.close_choose_reservation = self.findChild(QtWidgets.QPushButton, "close_choose_reservation")
        self.close_choose_reservation.setIcon(QtGui.QIcon('close_icon.png'))
        self.close_choose_reservation.setIconSize(QtCore.QSize(25, 25))

        self.your_reservatiion = self.findChild(QtWidgets.QPushButton, "your_reservation")
        self.your_reservatiion.setFont(QFont('Arial', 14, weight=QFont.Bold))

        self.reservation_a_table = self.findChild(QtWidgets.QPushButton, "reservation_a_table")
        self.reservation_a_table.setFont(QFont('Arial', 14, weight=QFont.Bold))

        self.background_choose_reservation = self.findChild(QtWidgets.QLabel, "background_choose_reservation")
        self.background_choose_reservation.setStyleSheet("background-color: lightgreen")

    def to_close_choose_reservation(self):
        self.close_choose_reservation.hide()
        self.your_reservatiion.hide()
        self.reservation_a_table.hide()
        self.background_choose_reservation.hide()


#         self.show()
#
# if __name__ == "__main__":
#     import sys
#     app = QtWidgets.QApplication(sys.argv)
#     new = choose_reservation()
#     sys.exit(app.exec_())