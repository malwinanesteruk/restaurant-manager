from datetime import datetime

from PyQt5 import QtCore, QtGui, QtWidgets, uic
from PyQt5.QtCore import *
from PyQt5.QtGui import *


class table_reservation(QtWidgets.QWidget):
    def __init__(self):
        super(table_reservation, self).__init__()  # Call the inherited classes __init__ method
        uic.loadUi("table_reservation.ui", self)  # Load the .ui file

        self.close_table_reservation = self.findChild(QtWidgets.QPushButton, "close_table_reservation")
        self.close_table_reservation.setIcon(QtGui.QIcon('close_icon.png'))
        self.close_table_reservation.setIconSize(QtCore.QSize(25, 25))

        self.communique_label_1 = self.findChild(QtWidgets.QLabel, "communique_label_1")

        self.current_year = datetime.now().year
        self.current_month = datetime.now().month
        self.current_day = datetime.now().day

        self.calendar = self.findChild(QtWidgets.QCalendarWidget, "calendar")
        self.calendar.setFont(QFont('Arial', 8, weight=QFont.Bold))
        self.calendar.setMinimumDate(QDate(self.current_year, self.current_month, self.current_day))
        if self.current_month == 12:
            if self.current_day == 31:
                self.calendar.setMaximumDate(QDate(self.current_year + 1, 2, self.current_day - 3))
            elif self.current_day == 30:
                self.calendar.setMaximumDate(QDate(self.current_year + 1, 2, self.current_day - 2))
            elif self.current_day == 29:
                self.calendar.setMaximumDate(QDate(self.current_year + 1, 2, self.current_day - 1))
            else:
                self.calendar.setMaximumDate(QDate(self.current_year + 1, 2, self.current_day))
        elif self.current_month == 11:
            if self.current_day == 30:
                self.calendar.setMaximumDate(QDate(self.current_year + 1, 1, self.current_day + 1))
            else:
                self.calendar.setMaximumDate(QDate(self.current_year + 1, 1, self.current_day))
        elif self.current_month == 7 and self.current_day == 31:
            self.calendar.setMaximumDate(QDate(self.current_year, self.current_month + 2, self.current_day - 1))
        elif self.current_month == 6 and self.current_day == 30:
            self.calendar.setMaximumDate(QDate(self.current_year, self.current_month + 2, self.current_day + 1))
        elif self.current_month == 2:
            if self.current_day == 29:
                self.calendar.setMaximumDate(QDate(self.current_year, self.current_month + 2, self.current_day + 1))
            elif self.current_day == 28:
                self.calendar.setMaximumDate(QDate(self.current_year, self.current_month + 2, self.current_day + 2))
        else:
            self.calendar.setMaximumDate(QDate(self.current_year, self.current_month + 2, self.current_day))
        self.calendar.clicked.connect(self.show_button)

        self.communique_label_2 = self.findChild(QtWidgets.QLabel, "communique_label_2")

        self.button_1 = self.findChild(QtWidgets.QPushButton, "button_1")
        self.button_1.setEnabled(False)
        self.button_1.clicked.connect(self.show_number_of_table_b1)
        self.button_1.setFont(QFont('Arial', 10, weight=QFont.Bold))

        self.button_2 = self.findChild(QtWidgets.QPushButton, "button_2")
        self.button_2.setEnabled(False)
        self.button_2.clicked.connect(self.show_number_of_table_b2)
        self.button_2.setFont(QFont('Arial', 10, weight=QFont.Bold))

        self.button_3 = self.findChild(QtWidgets.QPushButton, "button_3")
        self.button_3.setEnabled(False)
        self.button_3.clicked.connect(self.show_number_of_table_b3)
        self.button_3.setFont(QFont('Arial', 10, weight=QFont.Bold))

        self.button_4 = self.findChild(QtWidgets.QPushButton, "button_4")
        self.button_4.setEnabled(False)
        self.button_4.clicked.connect(self.show_number_of_table_b4)
        self.button_4.setFont(QFont('Arial', 10, weight=QFont.Bold))

        self.button_5 = self.findChild(QtWidgets.QPushButton, "button_5")
        self.button_5.setEnabled(False)
        self.button_5.clicked.connect(self.show_number_of_table_b5)
        self.button_5.setFont(QFont('Arial', 10, weight=QFont.Bold))

        self.button_6 = self.findChild(QtWidgets.QPushButton, "button_6")
        self.button_6.setEnabled(False)
        self.button_6.clicked.connect(self.show_number_of_table_b6)
        self.button_6.setFont(QFont('Arial', 10, weight=QFont.Bold))

        self.button_8 = self.findChild(QtWidgets.QPushButton, "button_8")
        self.button_8.setEnabled(False)
        self.button_8.clicked.connect(self.show_number_of_table_b8)
        self.button_8.setFont(QFont('Arial', 10, weight=QFont.Bold))

        self.button_10 = self.findChild(QtWidgets.QPushButton, "button_10")
        self.button_10.setEnabled(False)
        self.button_10.clicked.connect(self.show_number_of_table_b10)
        self.button_10.setFont(QFont('Arial', 10, weight=QFont.Bold))

        self.communique_label_3 = self.findChild(QtWidgets.QLabel, "communique_label_3")

        self.scroll_number_table = self.findChild(QtWidgets.QScrollArea, "scroll_number_table")

        self.number_of_table_1 = self.findChild(QtWidgets.QPushButton, "number_of_table_1")
        self.number_of_table_1.setEnabled(False)
        self.number_of_table_1.clicked.connect(self.show_table_reservation_table_1)
        self.number_of_table_1.setFont(QFont('Arial', 10, weight=QFont.Bold))

        self.number_of_table_2 = self.findChild(QtWidgets.QPushButton, "number_of_table_2")
        self.number_of_table_2.setEnabled(False)
        self.number_of_table_2.clicked.connect(self.show_table_reservation_table_2)
        self.number_of_table_2.setFont(QFont('Arial', 10, weight=QFont.Bold))

        self.number_of_table_3 = self.findChild(QtWidgets.QPushButton, "number_of_table_3")
        self.number_of_table_3.setEnabled(False)
        self.number_of_table_3.clicked.connect(self.show_table_reservation_table_3)
        self.number_of_table_3.setFont(QFont('Arial', 10, weight=QFont.Bold))

        self.number_of_table_4 = self.findChild(QtWidgets.QPushButton, "number_of_table_4")
        self.number_of_table_4.setEnabled(False)
        self.number_of_table_4.clicked.connect(self.show_table_reservation_table_4)
        self.number_of_table_4.setFont(QFont('Arial', 10, weight=QFont.Bold))

        self.number_of_table_5 = self.findChild(QtWidgets.QPushButton, "number_of_table_5")
        self.number_of_table_5.setEnabled(False)
        self.number_of_table_5.clicked.connect(self.show_table_reservation_table_5)
        self.number_of_table_5.setFont(QFont('Arial', 10, weight=QFont.Bold))

        self.number_of_table_6 = self.findChild(QtWidgets.QPushButton, "number_of_table_6")
        self.number_of_table_6.setEnabled(False)
        self.number_of_table_6.clicked.connect(self.show_table_reservation_table_6)
        self.number_of_table_6.setFont(QFont('Arial', 10, weight=QFont.Bold))

        self.number_of_table_7 = self.findChild(QtWidgets.QPushButton, "number_of_table_7")
        self.number_of_table_7.setEnabled(False)
        self.number_of_table_7.clicked.connect(self.show_table_reservation_table_7)
        self.number_of_table_7.setFont(QFont('Arial', 10, weight=QFont.Bold))

        self.number_of_table_8 = self.findChild(QtWidgets.QPushButton, "number_of_table_8")
        self.number_of_table_8.setEnabled(False)
        self.number_of_table_8.clicked.connect(self.show_table_reservation_table_8)
        self.number_of_table_8.setFont(QFont('Arial', 10, weight=QFont.Bold))

        self.number_of_table_9 = self.findChild(QtWidgets.QPushButton, "number_of_table_9")
        self.number_of_table_9.setEnabled(False)
        self.number_of_table_9.clicked.connect(self.show_table_reservation_table_9)
        self.number_of_table_9.setFont(QFont('Arial', 10, weight=QFont.Bold))

        self.number_of_table_10 = self.findChild(QtWidgets.QPushButton, "number_of_table_10")
        self.number_of_table_10.setEnabled(False)
        self.number_of_table_10.clicked.connect(self.show_table_reservation_table_10)
        self.number_of_table_10.setFont(QFont('Arial', 10, weight=QFont.Bold))

        self.number_of_table_11 = self.findChild(QtWidgets.QPushButton, "number_of_table_11")
        self.number_of_table_11.setEnabled(False)
        self.number_of_table_11.clicked.connect(self.show_table_reservation_table_11)
        self.number_of_table_11.setFont(QFont('Arial', 10, weight=QFont.Bold))

        self.number_of_table_12 = self.findChild(QtWidgets.QPushButton, "number_of_table_12")
        self.number_of_table_12.setEnabled(False)
        self.number_of_table_12.clicked.connect(self.show_table_reservation_table_12)
        self.number_of_table_12.setFont(QFont('Arial', 10, weight=QFont.Bold))

        self.communique_label_4 = self.findChild(QtWidgets.QLabel, "communique_label_4")

        self.comment_to_reservation = self.findChild(QtWidgets.QTextEdit, "comment_to_reservation")

        self.table_reservation = self.findChild(QtWidgets.QPushButton, "table_reservation")
        self.table_reservation.setFont(QFont('Arial', 14, weight=QFont.Bold))
        self.table_reservation.setEnabled(False)
        self.table_reservation.clicked.connect(self.open_end_table_reservation)

        self.background_table_reservation = self.findChild(QtWidgets.QLabel, "background_table_reservation")
        self.background_table_reservation.setStyleSheet("background-color: lightgreen")

    def show_button(self):
        self.reset_settings()

        self.calendar.setStyleSheet("selection-background-color: lightgreen")
        self.button_1.setEnabled(True)
        self.button_2.setEnabled(True)
        self.button_3.setEnabled(True)
        self.button_4.setEnabled(True)
        self.button_5.setEnabled(True)
        self.button_6.setEnabled(True)
        self.button_8.setEnabled(True)
        self.button_10.setEnabled(True)

        self.number_of_table_1.setEnabled(False)
        self.number_of_table_1.show()
        self.number_of_table_2.setEnabled(False)
        self.number_of_table_2.show()
        self.number_of_table_3.setEnabled(False)
        self.number_of_table_3.show()
        self.number_of_table_4.setEnabled(False)
        self.number_of_table_4.show()
        self.number_of_table_5.setEnabled(False)
        self.number_of_table_5.show()
        self.number_of_table_6.setEnabled(False)
        self.number_of_table_6.show()
        self.number_of_table_7.setEnabled(False)
        self.number_of_table_7.show()
        self.number_of_table_8.setEnabled(False)
        self.number_of_table_8.show()
        self.number_of_table_9.setEnabled(False)
        self.number_of_table_9.show()
        self.number_of_table_10.setEnabled(False)
        self.number_of_table_10.show()
        self.number_of_table_11.setEnabled(False)
        self.number_of_table_11.show()
        self.number_of_table_12.setEnabled(False)
        self.number_of_table_12.show()

    def reset_settings(self):
        self.number_of_table_1.setEnabled(False)
        self.number_of_table_1.show()
        self.number_of_table_2.setEnabled(False)
        self.number_of_table_2.show()
        self.number_of_table_3.setEnabled(False)
        self.number_of_table_3.show()
        self.number_of_table_4.setEnabled(False)
        self.number_of_table_4.show()
        self.number_of_table_5.setEnabled(False)
        self.number_of_table_5.show()
        self.number_of_table_6.setEnabled(False)
        self.number_of_table_6.show()
        self.number_of_table_7.setEnabled(False)
        self.number_of_table_7.show()
        self.number_of_table_8.setEnabled(False)
        self.number_of_table_8.show()
        self.number_of_table_9.setEnabled(False)
        self.number_of_table_9.show()
        self.number_of_table_10.setEnabled(False)
        self.number_of_table_10.show()
        self.number_of_table_11.setEnabled(False)
        self.number_of_table_11.show()
        self.number_of_table_12.setEnabled(False)
        self.number_of_table_12.show()

        self.button_1.setStyleSheet("")
        self.button_1.setEnabled(True)

        self.button_2.setStyleSheet("")
        self.button_2.setEnabled(True)

        self.button_3.setStyleSheet("")
        self.button_3.setEnabled(True)

        self.button_4.setStyleSheet("")
        self.button_4.setEnabled(True)

        self.button_5.setStyleSheet("")
        self.button_5.setEnabled(True)

        self.button_6.setStyleSheet("")
        self.button_6.setEnabled(True)

        self.button_8.setStyleSheet("")
        self.button_8.setEnabled(True)

        self.button_10.setStyleSheet("")
        self.button_10.setEnabled(True)

        self.reset_number_of_table()

        self.table_reservation.setEnabled(False)

    def show_number_of_table_b1(self):
        self.reset_settings()
        self.button_1.setStyleSheet("background-color: lightgreen")
        self.button_1.setEnabled(False)
        self.number_of_table_1.setEnabled(True)
        self.number_of_table_2.setEnabled(True)
        self.number_of_table_3.setEnabled(True)
        self.number_of_table_4.setEnabled(True)
        self.number_of_table_5.setEnabled(True)
        self.number_of_table_6.setEnabled(True)
        self.number_of_table_7.setEnabled(False)
        self.number_of_table_7.hide()
        self.number_of_table_8.setEnabled(False)
        self.number_of_table_8.hide()
        self.number_of_table_9.setEnabled(False)
        self.number_of_table_9.hide()
        self.number_of_table_10.setEnabled(False)
        self.number_of_table_10.hide()
        self.number_of_table_11.setEnabled(False)
        self.number_of_table_11.hide()
        self.number_of_table_12.setEnabled(False)
        self.number_of_table_12.hide()

    def show_number_of_table_b2(self):
        self.reset_settings()
        self.button_2.setStyleSheet("background-color: lightgreen")
        self.button_2.setEnabled(False)
        self.number_of_table_1.setEnabled(True)
        self.number_of_table_2.setEnabled(True)
        self.number_of_table_3.setEnabled(True)
        self.number_of_table_4.setEnabled(True)
        self.number_of_table_5.setEnabled(True)
        self.number_of_table_6.setEnabled(True)
        self.number_of_table_7.setEnabled(True)
        self.number_of_table_8.setEnabled(True)
        self.number_of_table_9.setEnabled(True)
        self.number_of_table_10.setEnabled(True)
        self.number_of_table_11.setEnabled(True)
        self.number_of_table_12.setEnabled(True)

    def show_number_of_table_b3(self):
        self.reset_settings()
        self.button_3.setStyleSheet("background-color: lightgreen")
        self.button_3.setEnabled(False)
        self.number_of_table_1.setEnabled(True)
        self.number_of_table_2.setEnabled(True)
        self.number_of_table_3.setEnabled(True)
        self.number_of_table_4.setEnabled(True)
        self.number_of_table_5.setEnabled(True)
        self.number_of_table_6.setEnabled(True)
        self.number_of_table_7.setEnabled(False)
        self.number_of_table_7.hide()
        self.number_of_table_8.setEnabled(False)
        self.number_of_table_8.hide()
        self.number_of_table_9.setEnabled(False)
        self.number_of_table_9.hide()
        self.number_of_table_10.setEnabled(False)
        self.number_of_table_10.hide()
        self.number_of_table_11.setEnabled(False)
        self.number_of_table_11.hide()
        self.number_of_table_12.setEnabled(False)
        self.number_of_table_12.hide()

    def show_number_of_table_b4(self):
        self.reset_settings()
        self.button_4.setStyleSheet("background-color: lightgreen")
        self.button_4.setEnabled(False)
        self.number_of_table_1.setEnabled(True)
        self.number_of_table_2.setEnabled(True)
        self.number_of_table_3.setEnabled(True)
        self.number_of_table_4.setEnabled(True)
        self.number_of_table_5.setEnabled(True)
        self.number_of_table_6.setEnabled(True)
        self.number_of_table_7.setEnabled(True)
        self.number_of_table_8.setEnabled(True)
        self.number_of_table_9.setEnabled(True)
        self.number_of_table_10.setEnabled(True)
        self.number_of_table_11.setEnabled(False)
        self.number_of_table_11.hide()
        self.number_of_table_12.setEnabled(False)
        self.number_of_table_12.hide()

    def show_number_of_table_b5(self):
        self.reset_settings()
        self.button_5.setStyleSheet("background-color: lightgreen")
        self.button_5.setEnabled(False)
        self.number_of_table_1.setEnabled(True)
        self.number_of_table_2.setEnabled(True)
        self.number_of_table_3.setEnabled(True)
        self.number_of_table_4.setEnabled(True)
        self.number_of_table_5.setEnabled(True)
        self.number_of_table_6.setEnabled(False)
        self.number_of_table_6.hide()
        self.number_of_table_7.setEnabled(False)
        self.number_of_table_7.hide()
        self.number_of_table_8.setEnabled(False)
        self.number_of_table_8.hide()
        self.number_of_table_9.setEnabled(False)
        self.number_of_table_9.hide()
        self.number_of_table_10.setEnabled(False)
        self.number_of_table_10.hide()
        self.number_of_table_11.setEnabled(False)
        self.number_of_table_11.hide()
        self.number_of_table_12.setEnabled(False)
        self.number_of_table_12.hide()

    def show_number_of_table_b6(self):
        self.reset_settings()
        self.button_6.setStyleSheet("background-color: lightgreen")
        self.button_6.setEnabled(False)
        self.number_of_table_1.setEnabled(True)
        self.number_of_table_2.setEnabled(True)
        self.number_of_table_3.setEnabled(True)
        self.number_of_table_4.setEnabled(True)
        self.number_of_table_5.setEnabled(True)
        self.number_of_table_6.setEnabled(False)
        self.number_of_table_6.hide()
        self.number_of_table_7.setEnabled(False)
        self.number_of_table_7.hide()
        self.number_of_table_8.setEnabled(False)
        self.number_of_table_8.hide()
        self.number_of_table_9.setEnabled(False)
        self.number_of_table_9.hide()
        self.number_of_table_10.setEnabled(False)
        self.number_of_table_10.hide()
        self.number_of_table_11.setEnabled(False)
        self.number_of_table_11.hide()
        self.number_of_table_12.setEnabled(False)
        self.number_of_table_12.hide()

    def show_number_of_table_b8(self):
        self.reset_settings()
        self.button_8.setStyleSheet("background-color: lightgreen")
        self.button_8.setEnabled(False)
        self.number_of_table_1.setEnabled(True)
        self.number_of_table_2.setEnabled(True)
        self.number_of_table_3.setEnabled(True)
        self.number_of_table_4.setEnabled(True)
        self.number_of_table_5.setEnabled(False)
        self.number_of_table_5.hide()
        self.number_of_table_6.setEnabled(False)
        self.number_of_table_6.hide()
        self.number_of_table_7.setEnabled(False)
        self.number_of_table_7.hide()
        self.number_of_table_8.setEnabled(False)
        self.number_of_table_8.hide()
        self.number_of_table_9.setEnabled(False)
        self.number_of_table_9.hide()
        self.number_of_table_10.setEnabled(False)
        self.number_of_table_10.hide()
        self.number_of_table_11.setEnabled(False)
        self.number_of_table_11.hide()
        self.number_of_table_12.setEnabled(False)
        self.number_of_table_12.hide()

    def show_number_of_table_b10(self):
        self.reset_settings()
        self.button_10.setStyleSheet("background-color: lightgreen")
        self.button_10.setEnabled(False)
        self.number_of_table_1.setEnabled(True)
        self.number_of_table_2.setEnabled(True)
        self.number_of_table_3.setEnabled(True)
        self.number_of_table_4.setEnabled(True)
        self.number_of_table_5.setEnabled(False)
        self.number_of_table_5.hide()
        self.number_of_table_6.setEnabled(False)
        self.number_of_table_6.hide()
        self.number_of_table_7.setEnabled(False)
        self.number_of_table_7.hide()
        self.number_of_table_8.setEnabled(False)
        self.number_of_table_8.hide()
        self.number_of_table_9.setEnabled(False)
        self.number_of_table_9.hide()
        self.number_of_table_10.setEnabled(False)
        self.number_of_table_10.hide()
        self.number_of_table_11.setEnabled(False)
        self.number_of_table_11.hide()
        self.number_of_table_12.setEnabled(False)
        self.number_of_table_12.hide()

    def reset_number_of_table(self):
        self.number_of_table_1.setStyleSheet("")
        self.number_of_table_1.setEnabled(True)

        self.number_of_table_2.setStyleSheet("")
        self.number_of_table_2.setEnabled(True)

        self.number_of_table_3.setStyleSheet("")
        self.number_of_table_3.setEnabled(True)

        self.number_of_table_4.setStyleSheet("")
        self.number_of_table_4.setEnabled(True)

        self.number_of_table_5.setStyleSheet("")
        self.number_of_table_5.setEnabled(True)

        self.number_of_table_6.setStyleSheet("")
        self.number_of_table_6.setEnabled(True)

        self.number_of_table_7.setStyleSheet("")
        self.number_of_table_7.setEnabled(True)

        self.number_of_table_8.setStyleSheet("")
        self.number_of_table_8.setEnabled(True)

        self.number_of_table_9.setStyleSheet("")
        self.number_of_table_9.setEnabled(True)

        self.number_of_table_10.setStyleSheet("")
        self.number_of_table_10.setEnabled(True)

        self.number_of_table_11.setStyleSheet("")
        self.number_of_table_11.setEnabled(True)

        self.number_of_table_12.setStyleSheet("")
        self.number_of_table_12.setEnabled(True)

    def show_table_reservation_table_1(self):
        self.reset_number_of_table()

        self.table_reservation.setEnabled(True)
        self.number_of_table_1.setStyleSheet("background-color: lightgreen")
        self.number_of_table_1.setEnabled(False)

    def show_table_reservation_table_2(self):
        self.reset_number_of_table()

        self.table_reservation.setEnabled(True)
        self.number_of_table_2.setStyleSheet("background-color: lightgreen")
        self.number_of_table_2.setEnabled(False)

    def show_table_reservation_table_3(self):
        self.reset_number_of_table()

        self.table_reservation.setEnabled(True)
        self.number_of_table_3.setStyleSheet("background-color: lightgreen")
        self.number_of_table_3.setEnabled(False)

    def show_table_reservation_table_4(self):
        self.reset_number_of_table()

        self.table_reservation.setEnabled(True)
        self.number_of_table_4.setStyleSheet("background-color: lightgreen")
        self.number_of_table_4.setEnabled(False)

    def show_table_reservation_table_5(self):
        self.reset_number_of_table()

        self.table_reservation.setEnabled(True)
        self.number_of_table_5.setStyleSheet("background-color: lightgreen")
        self.number_of_table_5.setEnabled(False)

    def show_table_reservation_table_6(self):
        self.reset_number_of_table()

        self.table_reservation.setEnabled(True)
        self.number_of_table_6.setStyleSheet("background-color: lightgreen")
        self.number_of_table_6.setEnabled(False)

    def show_table_reservation_table_7(self):
        self.reset_number_of_table()

        self.table_reservation.setEnabled(True)
        self.number_of_table_7.setStyleSheet("background-color: lightgreen")
        self.number_of_table_7.setEnabled(False)

    def show_table_reservation_table_8(self):
        self.reset_number_of_table()

        self.table_reservation.setEnabled(True)
        self.number_of_table_8.setStyleSheet("background-color: lightgreen")
        self.number_of_table_8.setEnabled(False)

    def show_table_reservation_table_9(self):
        self.reset_number_of_table()

        self.table_reservation.setEnabled(True)
        self.number_of_table_9.setStyleSheet("background-color: lightgreen")
        self.number_of_table_9.setEnabled(False)

    def show_table_reservation_table_10(self):
        self.reset_number_of_table()

        self.table_reservation.setEnabled(True)
        self.number_of_table_10.setStyleSheet("background-color: lightgreen")
        self.number_of_table_10.setEnabled(False)

    def show_table_reservation_table_11(self):
        self.reset_number_of_table()

        self.table_reservation.setEnabled(True)
        self.number_of_table_11.setStyleSheet("background-color: lightgreen")
        self.number_of_table_11.setEnabled(False)

    def show_table_reservation_table_12(self):
        self.reset_number_of_table()

        self.table_reservation.setEnabled(True)
        self.number_of_table_12.setStyleSheet("background-color: lightgreen")
        self.number_of_table_12.setEnabled(False)

    def open_end_table_reservation(self):
        self.parent().parent().parent().parent().parent().parent().parent().show_end_table_reservation()

    def close_all_widget_table_reservation(self):
        self.close_table_reservation.hide()
        self.communique_label_1.hide()
        self.calendar.hide()
        self.communique_label_2.hide()
        self.button_1.hide()
        self.button_2.hide()
        self.button_3.hide()
        self.button_4.hide()
        self.button_5.hide()
        self.button_6.hide()
        self.button_8.hide()
        self.button_10.hide()
        self.communique_label_3.hide()
        self.scroll_number_table.hide()
        self.number_of_table_1.hide()
        self.number_of_table_2.hide()
        self.number_of_table_3.hide()
        self.number_of_table_4.hide()
        self.number_of_table_5.hide()
        self.number_of_table_6.hide()
        self.number_of_table_7.hide()
        self.number_of_table_8.hide()
        self.number_of_table_9.hide()
        self.number_of_table_10.hide()
        self.number_of_table_11.hide()
        self.number_of_table_12.hide()
        self.communique_label_4.hide()
        self.comment_to_reservation.hide()
        self.table_reservation.hide()
        self.background_table_reservation.hide()

#         self.show()
#
# if __name__ == "__main__":
#     import sys
#     app = QtWidgets.QApplication(sys.argv)
#     new = table_reservation()
#     sys.exit(app.exec_())