# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'registration.ui'
#
# Created by: PyQt5 UI code generator 5.15.0
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets, uic

class edit_data(QtWidgets.QWidget):
    def __init__(self):
        super(edit_data, self).__init__()  # Call the inherited classes __init__ method
        uic.loadUi("edit_data.ui", self)  # Load the .ui file

        self.background_edit_data = self.findChild(QtWidgets.QLabel, "background_edit_data")
        self.background_edit_data.setStyleSheet("background-color: lightgreen")

        self.close_edit_data = self.findChild(QtWidgets.QPushButton, "close_edit_data")
        self.close_edit_data.setIcon(QtGui.QIcon('close_icon.png'))
        self.close_edit_data.setIconSize(QtCore.QSize(30, 30))
        #self.close_edit_data.clicked.connect(self.close_to_edit_data)

        self.edit_data_label = self.findChild(QtWidgets.QLabel, "edit_data_label")

        self.error_message_label = self.findChild(QtWidgets.QLabel, "error_message_label")

        self.name_line = self.findChild(QtWidgets.QLineEdit, "name_line")

        self.name_label = self.findChild(QtWidgets.QLabel, "name_label")

        self.surname_line = self.findChild(QtWidgets.QLineEdit, "surname_line")

        self.surname_label = self.findChild(QtWidgets.QLabel, "surname_label")

        self.street_line = self.findChild(QtWidgets.QLineEdit, "street_line")

        self.street_label = self.findChild(QtWidgets.QLabel, "street_label")

        self.street_number_line = self.findChild(QtWidgets.QLineEdit, "street_number_line")
        self.street_number_line.setMaxLength(10)

        self.street_number_label = self.findChild(QtWidgets.QLabel, "street_number_label")

        self.apartment_number_line = self.findChild(QtWidgets.QLineEdit, "apartment_number_line")
        self.apartment_number_line.setMaxLength(4)

        self.apartment_number_label = self.findChild(QtWidgets.QLabel, "apartment_number_label")

        self.edit_data_button = self.findChild(QtWidgets.QPushButton, "edit_data_button")
        self.edit_data_button.clicked.connect(self.edit_data_method)

        self.communique_background_label = self.findChild(QtWidgets.QLabel, "communique_background_label")
        self.communique_background_label.setStyleSheet("background-color: lightgreen")
        self.communique_background_label.hide()

        self.communique_label = self.findChild(QtWidgets.QLabel, "communique_label")
        self.communique_label.hide()

        self.communique_yes_button = self.findChild(QtWidgets.QPushButton, "communique_yes")
        # self.communique_yes_button.clicked.connect(self.communique_yes_method)
        self.communique_yes_button.hide()

        self.communique_no_button = self.findChild(QtWidgets.QPushButton, "communique_no")
        self.communique_no_button.clicked.connect(self.communique_no_method)
        self.communique_no_button.hide()

        # self.show()

    def close_to_edit_data(self):
        self.communique_background_label.hide()
        self.communique_label.hide()
        self.communique_yes_button.hide()
        self.communique_no_button.hide()

        self.close_edit_data.hide()
        self.edit_data_label.hide()
        self.background_edit_data.hide()
        self.error_message_label.hide()
        self.name_line.hide()
        self.name_label.hide()
        self.surname_line.hide()
        self.surname_label.hide()
        self.street_line.hide()
        self.street_label.hide()
        self.street_number_line.hide()
        self.street_number_label.hide()
        self.apartment_number_line.hide()
        self.apartment_number_label.hide()
        self.edit_data_button.hide()

    def edit_data_method(self):
        if self.name_line.text() == "" or self.surname_line.text() == "" or self.street_line.text() == "" or self.street_number_line.text() == "" or self.apartment_number_line.text() == "":
            self.error_message_label.setText("Uzupełnij brakujące dane.")
            self.error_message_label.show()
        else:
            self.close_edit_data.hide()
            self.edit_data_label.hide()
            self.background_edit_data.hide()
            self.error_message_label.hide()
            self.name_line.hide()
            self.name_label.hide()
            self.surname_line.hide()
            self.surname_label.hide()
            self.street_line.hide()
            self.street_label.hide()
            self.street_number_line.hide()
            self.street_number_label.hide()
            self.apartment_number_line.hide()
            self.apartment_number_label.hide()
            self.edit_data_button.hide()

            self.communique_background_label.show()
            self.communique_label.show()
            self.communique_yes_button.show()
            self.communique_no_button.show()

    def communique_yes_method(self):
        self.communique_background_label.hide()
        self.communique_label.hide()
        self.communique_yes_button.hide()
        self.communique_no_button.hide()

        self.close_edit_data.hide()
        self.edit_data_label.hide()
        self.background_edit_data.hide()
        self.error_message_label.hide()
        self.name_line.hide()
        self.name_label.hide()
        self.surname_line.hide()
        self.surname_label.hide()
        self.street_line.hide()
        self.street_label.hide()
        self.street_number_line.hide()
        self.street_number_label.hide()
        self.apartment_number_line.hide()
        self.apartment_number_label.hide()
        self.edit_data_button.hide()

    def communique_no_method(self):
        self.communique_background_label.hide()
        self.communique_label.hide()
        self.communique_yes_button.hide()
        self.communique_no_button.hide()

        self.close_edit_data.show()
        self.edit_data_label.show()
        self.background_edit_data.show()
        self.error_message_label.show()
        self.name_line.show()
        self.name_label.show()
        self.surname_line.show()
        self.surname_label.show()
        self.street_line.show()
        self.street_label.show()
        self.street_number_line.show()
        self.street_number_label.show()
        self.apartment_number_line.show()
        self.apartment_number_label.show()
        self.edit_data_button.show()

# if __name__ == "__main__":
#     import sys
#     app = QtWidgets.QApplication(sys.argv)
#     new = edit_data()
#     sys.exit(app.exec_())
