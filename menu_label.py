from PyQt5 import QtCore, QtGui, QtWidgets, uic

class menu_label(QtWidgets.QWidget):
    def __init__(self):
        super(menu_label, self).__init__()  # Call the inherited classes __init__ method
        uic.loadUi("menu_label.ui", self)  # Load the .ui file

        self.category_name_label = self.findChild(QtWidgets.QLabel, "category_name_label")

#         self.show()
#
# if __name__ == "__main__":
#     import sys
#     app = QtWidgets.QApplication(sys.argv)
#     new = menu_label()
#     sys.exit(app.exec_())