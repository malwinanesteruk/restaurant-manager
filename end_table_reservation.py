from PyQt5 import QtCore, QtGui, QtWidgets, uic
from PyQt5.QtCore import *
from PyQt5.QtGui import *


class end_table_reservation(QtWidgets.QWidget):
    def __init__(self):
        super(end_table_reservation, self).__init__()  # Call the inherited classes __init__ method
        uic.loadUi("end_table_reservation.ui", self)  # Load the .ui file

        self.communique_end_table_reservation = self.findChild(QtWidgets.QLabel, "communique_end_table_reservation")

        self.background_communique_end_table_reservation = self.findChild(QtWidgets.QLabel, "background_communique_end_table_reservation")
        self.background_communique_end_table_reservation.setStyleSheet("background-color: lightgreen")

        self.close_end_table_reservation = self.findChild(QtWidgets.QPushButton, "close_end_table_reservation")
        self.close_end_table_reservation.setIcon(QtGui.QIcon('close_icon.png'))
        self.close_end_table_reservation.setIconSize(QtCore.QSize(25, 25))
        self.close_end_table_reservation.clicked.connect(self.to_close_end_table_reservation)
        #
        # self.show()

    def to_close_end_table_reservation(self):
        self.communique_end_table_reservation.hide()
        self.background_communique_end_table_reservation.hide()
        self.close_end_table_reservation.hide()
        self.parent().parent().parent().closed_end_table_reservation()

# if __name__ == "__main__":
#     import sys
#     app = QtWidgets.QApplication(sys.argv)
#     new = end_table_reservation()
#     sys.exit(app.exec_())