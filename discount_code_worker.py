from PyQt5 import QtCore, QtGui, QtWidgets, uic
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from scroll_area import scroll_area
from datetime import datetime

class discount_code_worker(QtWidgets.QWidget):
    def __init__(self):
        super(discount_code_worker, self).__init__()  # Call the inherited classes __init__ method
        uic.loadUi("discount_code_worker.ui", self)

        self.communique_label_1 = self.findChild(QtWidgets.QLabel, "communique_label_1")

        self.code_name = self.findChild(QtWidgets.QLineEdit, "code_name")
        self.code_name.textChanged.connect(self.show_discount_value)

        self.discount_value = self.findChild(QtWidgets.QSpinBox, "discount_value")
        self.discount_value.setEnabled(False)
        self.discount_value.valueChanged.connect(self.show_calendar)

        self.percent = self.findChild(QtWidgets.QLabel, "percent")
        self.percent.setEnabled(False)

        self.communique_label_2 = self.findChild(QtWidgets.QLabel, "communique_label_2")

        self.current_year = datetime.now().year
        self.current_month = datetime.now().month
        self.current_day = datetime.now().day

        self.calendar_code = self.findChild(QtWidgets.QCalendarWidget, "calendar_code")
        self.calendar_code.setFont(QFont('Arial', 8, weight=QFont.Bold))
        self.calendar_code.setMinimumDate(QDate(self.current_year, self.current_month, self.current_day))
        self.calendar_code.setMaximumDate(QDate(2200, self.current_month, self.current_day))
        self.calendar_code.setEnabled(False)
        self.calendar_code.clicked.connect(self.show_button)

        self.communique_label_3 = self.findChild(QtWidgets.QLabel, "communique_label_3")

        self.calendar_code_2 = self.findChild(QtWidgets.QCalendarWidget, "calendar_code_2")
        self.calendar_code_2.setFont(QFont('Arial', 8, weight=QFont.Bold))
        self.calendar_code_2.setMinimumDate(QDate(self.current_year, self.current_month, self.current_day))
        self.calendar_code_2.setEnabled(False)
        self.calendar_code_2.clicked.connect(self.show_button_2)

        self.see_discount_code_button = self.findChild(QtWidgets.QPushButton, "see_discount_code_button")
        # self.see_discount_code_button.clicked.connect(self.show_list_of_generated_code)

        self.discount_code_button = self.findChild(QtWidgets.QPushButton, "discount_code_button")
        self.discount_code_button.setEnabled(False)
        self.discount_code_button.clicked.connect(self.open_end_generate_code)

        self.close_discount_code_worker = self.findChild(QtWidgets.QPushButton, "close_discount_code_worker")
        self.close_discount_code_worker.setIcon(QtGui.QIcon('close_icon.png'))
        self.close_discount_code_worker.setIconSize(QtCore.QSize(25, 25))
        # self.close_discount_code_worker.clicked.connect(self.close_to_discount_code_worker)

        self.background_discount_code_worker = self.findChild(QtWidgets.QLabel, "background_discount_code_worker")
        self.background_discount_code_worker.setStyleSheet("background-color: lightgreen")

        self.show()

    def show_discount_value(self):
        if self.code_name.text() == "":
            self.discount_value.setValue(0)
            self.discount_value.setEnabled(False)
            self.percent.setEnabled(False)

            self.calendar_code.setEnabled(False)
            self.calendar_code.setSelectedDate(QDate(self.current_year, self.current_month, self.current_day))
            self.calendar_code.setMinimumDate(QDate(self.current_year, self.current_month, self.current_day))
            self.calendar_code.setMaximumDate(QDate(2200, self.current_month, self.current_day))
            self.calendar_code.setStyleSheet("selection-background-color: ")

            self.calendar_code_2.setEnabled(False)
            self.calendar_code_2.setMinimumDate(self.calendar_code.selectedDate())
            self.calendar_code_2.setSelectedDate(self.calendar_code.selectedDate())
            self.calendar_code_2.setStyleSheet("selection-background-color: ")

            self.discount_code_button.setEnabled(False)
        else:
            self.discount_value.setEnabled(True)
            self.percent.setEnabled(True)

    def show_calendar(self):
        if self.discount_value.value() >= 1:
            self.calendar_code.setEnabled(True)
        else:
            self.calendar_code.setEnabled(False)
            self.calendar_code.setSelectedDate(QDate(self.current_year, self.current_month, self.current_day))
            self.calendar_code.setMinimumDate(QDate(self.current_year, self.current_month, self.current_day))
            self.calendar_code.setMaximumDate(QDate(2200, self.current_month, self.current_day))
            self.calendar_code.setStyleSheet("selection-background-color: ")

            self.calendar_code_2.setEnabled(False)
            self.calendar_code_2.setMinimumDate(self.calendar_code.selectedDate())
            self.calendar_code_2.setSelectedDate(self.calendar_code.selectedDate())
            self.calendar_code_2.setStyleSheet("selection-background-color: ")

            self.discount_code_button.setEnabled(False)

    def show_button(self):
        self.calendar_code.setStyleSheet("selection-background-color: lightgreen")
        self.calendar_code_2.setMinimumDate(self.calendar_code.selectedDate())
        self.calendar_code_2.setEnabled(True)

    def show_button_2(self):
        self.calendar_code_2.setStyleSheet("selection-background-color: lightgreen")
        self.calendar_code.setMaximumDate(self.calendar_code_2.selectedDate())
        self.discount_code_button.setEnabled(True)

    def open_end_generate_code(self):
        self.parent().parent().parent().show_end_generate_code()

    def close_to_discount_code_worker(self):
        self.communique_label_1.hide()
        self.code_name.hide()
        self.communique_label_2.hide()
        self.calendar_code.hide()
        self.discount_code_button.hide()
        self.close_discount_code_worker.hide()
        self.background_discount_code_worker.hide()

# if __name__ == "__main__":
#     import sys
#     app = QtWidgets.QApplication(sys.argv)
#     new = discount_code_worker()
#     sys.exit(app.exec_())