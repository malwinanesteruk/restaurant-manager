from PyQt5 import QtCore, QtGui, QtWidgets, uic
from PyQt5.QtCore import *
from PyQt5.QtGui import *


class details_rated_meal(QtWidgets.QWidget):
    def __init__(self):
        super(details_rated_meal, self).__init__()  # Call the inherited classes __init__ method
        uic.loadUi("details_rated_meal.ui", self)

        self.background_details_rated_meal = self.findChild(QtWidgets.QLabel, "background_details_rated_meal")
        self.background_details_rated_meal.setStyleSheet("background-color: lightgreen")

#         self.show()
#
# if __name__ == "__main__":
#     import sys
#     app = QtWidgets.QApplication(sys.argv)
#     new = details_rated_meal()
#     sys.exit(app.exec_())
#
