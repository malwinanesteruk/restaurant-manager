from PyQt5 import QtCore, QtGui, QtWidgets, uic
from PyQt5.QtCore import *
from PyQt5.QtGui import *


class rated_meal(QtWidgets.QWidget):
    def __init__(self):
        super(rated_meal, self).__init__()  # Call the inherited classes __init__ method
        uic.loadUi("rated_meal.ui", self)

        self.name_product = self.findChild(QtWidgets.QLabel, "name_product")

        self.average_grade = self.findChild(QtWidgets.QLabel, "average_grade")

        self.average_grade_change = self.findChild(QtWidgets.QLabel, "average_grade_change")

        self.background_rated_meal = self.findChild(QtWidgets.QLabel, "background_rated_meal")
        self.background_rated_meal.setStyleSheet("background-color: lightgreen")

        self.show_details_rated = self.findChild(QtWidgets.QPushButton, "show_details_rated")

#         self.show()
#
# if __name__ == "__main__":
#     import sys
#     app = QtWidgets.QApplication(sys.argv)
#     new = rated_meal()
#     sys.exit(app.exec_())