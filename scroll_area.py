from PyQt5 import QtCore, QtGui, QtWidgets, uic
from your_order import your_order
from discount_coupon import discount_coupon
from your_rate import your_rate
from product import product
from menu_label import menu_label
from menu_meal import menu_meal
from orders_worker import orders_worker
from details import details
from table_reservation import table_reservation
from your_reservation import your_reservation
from rated_meal import rated_meal
from details_rated_meal import details_rated_meal
from list_of_reservation import list_of_reservation
from list_of_generated_code import list_of_generated_code
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import (QWidget, QVBoxLayout)


class scroll_area(QtWidgets.QWidget):
    def __init__(self):
        super(scroll_area, self).__init__()
        uic.loadUi("scroll_area.ui", self)

        self.scroll = self.findChild(QtWidgets.QScrollArea, "scroll_area")

        self.widget = QWidget()
        self.vbox = QVBoxLayout()

        self.widget.setLayout(self.vbox)
        self.scroll.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        self.scroll.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.scroll.setWidgetResizable(True)

        self.scroll.setWidget(self.widget)

        self.your_order_object = your_order()

        # self.to_show_list_of_generated_code()

        #self.new_order_method()
        # self.note_method()
        # self.new_coupon_method()
        # self.new_rate_method()
        # self.to_show_menu()

        # self.to_show_table_reservation()
        # self.to_show_your_reservation()
        #self.to_show_details_order()
        # self.to_show_all_orders()
        # self.to_show_client_information()
        # self.to_show_client_orders()
        # self.to_show_client_order_details()
        # self.to_show_list_of_rated_meals()
        # self.to_show_details_rate_meal()
        # self.to_show_list_of_reservation()
        # self.show()

    def new_order_method(self):
        # self.parent().return_button.hide()
        for i in range(1, 10):
            self.your_order_object = your_order()
            self.your_order_object.details_of_the_order_button.clicked.connect(self.to_open_details)
            self.vbox.addWidget(self.your_order_object)

    def new_coupon_method(self):
        for i in range(1, 10):
            discount_coupon_object = discount_coupon()
            self.vbox.addWidget(discount_coupon_object)

    def new_rate_method(self):
        for i in range(1, 10):
            your_rate_object = your_rate()
            self.vbox.addWidget(your_rate_object)

    def details_method(self):
        # discount_coupon_object = your_rate()
        self.your_order_object.details_of_the_order_button.hide()
        self.vbox.addWidget(self.your_order_object)

        for i in range(1, 10):
            product_object = product()
            self.vbox.addWidget(product_object)

    def to_open_details(self):
        self.parent().parent().parent().return_button.hide()
        self.parent().parent().parent().return_button_2.show()
        for i in reversed(range(self.vbox.count())):
            self.vbox.itemAt(i).widget().setParent(None)
        self.details_method()

    def to_close_details(self):
        for i in reversed(range(self.vbox.count())):
            self.vbox.itemAt(i).widget().setParent(None)

    def to_show_menu(self):
        menu_label_object_1 = menu_label()
        menu_label_object_1.category_name_label.setText("ZUPA")
        menu_label_object_1.category_name_label.show()
        self.vbox.addWidget(menu_label_object_1)

        for i in range(1, 6):
            menu_meal_object_1 = menu_meal()
            menu_meal_object_1.setParent(self)
            self.vbox.addWidget(menu_meal_object_1)

        menu_label_object_2 = menu_label()
        menu_label_object_2.category_name_label.setText("DANIE GŁÓWNE")
        menu_label_object_2.category_name_label.show()
        self.vbox.addWidget(menu_label_object_2)

        for i in range(1, 15):
            menu_meal_object_2 = menu_meal()
            self.vbox.addWidget(menu_meal_object_2)

        menu_label_object_3 = menu_label()
        menu_label_object_3.category_name_label.setText("DODATKI")
        menu_label_object_3.category_name_label.show()
        self.vbox.addWidget(menu_label_object_3)

        for i in range(1, 10):
            menu_meal_object_3 = menu_meal()
            self.vbox.addWidget(menu_meal_object_3)

        menu_label_object_4 = menu_label()
        menu_label_object_4.category_name_label.setText("SURÓWKI")
        menu_label_object_4.category_name_label.show()
        self.vbox.addWidget(menu_label_object_4)

        for i in range(1, 8):
            menu_meal_object_4 = menu_meal()
            self.vbox.addWidget(menu_meal_object_4)

        menu_label_object_5 = menu_label()
        menu_label_object_5.category_name_label.setText("DESER")
        menu_label_object_5.category_name_label.show()
        self.vbox.addWidget(menu_label_object_5)

        for i in range(1, 10):
            menu_meal_object_5 = menu_meal()
            self.vbox.addWidget(menu_meal_object_5)

        menu_label_object_6 = menu_label()
        menu_label_object_6.category_name_label.setText("MENU DLA DZIECI")
        menu_label_object_6.category_name_label.show()
        self.vbox.addWidget(menu_label_object_6)

        for i in range(1, 6):
            menu_meal_object_6 = menu_meal()
            self.vbox.addWidget(menu_meal_object_6)

        menu_label_object_7 = menu_label()
        menu_label_object_7.category_name_label.setText("NAPOJE")
        menu_label_object_7.category_name_label.show()
        self.vbox.addWidget(menu_label_object_7)

        for i in range(1, 20):
            menu_meal_object_7 = menu_meal()
            self.vbox.addWidget(menu_meal_object_7)

    def to_show_table_reservation(self):
        self.table_reservation_object = table_reservation()
        self.table_reservation_object.close_table_reservation.clicked.connect(self.close_window)
        self.vbox.addWidget(self.table_reservation_object)

    def close_window(self):
        self.table_reservation_object.close_all_widget_table_reservation()
        self.parent().parent().parent().close_table_reservation()

    def to_show_your_reservation(self):
        for i in range(1, 10):
            self.your_reservation_object = your_reservation()
            self.vbox.addWidget(self.your_reservation_object)

    ############################################## STREFA PRACOWNIKA
    def to_show_all_orders(self):
        for i in range(1, 10):
            self.orders_worker_object = orders_worker()
            self.orders_worker_object.details_of_the_order_button.clicked.connect(self.to_show_details_order)
            self.vbox.addWidget(self.orders_worker_object)

    def to_show_details_order(self):
        self.parent().parent().parent().return_button.hide()
        self.parent().parent().parent().return_button_3.show()
        for i in reversed(range(self.vbox.count())):
            self.vbox.itemAt(i).widget().setParent(None)
        self.details_order_method()

    def details_order_method(self):
        self.orders_worker_object = orders_worker()
        self.orders_worker_object.details_of_the_order_button.hide()
        self.vbox.addWidget(self.orders_worker_object)

        self.details_object = details()
        self.details_object.orders_button.hide()
        self.vbox.addWidget(self.details_object)

        for i in range(1, 10):
            self.product_object = product()
            self.vbox.addWidget(self.product_object)

    def to_show_client_information(self):
        for i in range(1, 10):
            self.details_object = details()
            self.details_object.orders_button.show()
            self.vbox.addWidget(self.details_object)

    def to_show_client_orders(self):
        self.details_object = details()
        self.details_object.orders_button.hide()
        self.vbox.addWidget(self.details_object)
        for i in range(1, 10):
            self.your_order_object = your_order()
            self.vbox.addWidget(self.your_order_object)

    def to_show_client_order_details(self):
        self.details_object = details()
        self.details_object.orders_button.hide()
        self.vbox.addWidget(self.details_object)
        self.your_order_object = your_order()
        self.your_order_object.details_of_the_order_button.hide()
        self.vbox.addWidget(self.your_order_object)
        for i in range(1, 10):
            self.product_object = product()
            self.vbox.addWidget(self.product_object)
#########################################################################
    def to_show_list_of_rated_meals(self):
        for i in range(1, 10):
            self.rated_meal_object = rated_meal()
            self.rated_meal_object.show_details_rated.clicked.connect(self.to_show_details_rate_meal)
            self.vbox.addWidget(self.rated_meal_object)

    def to_show_details_rate_meal(self):
        self.parent().parent().parent().return_button.hide()
        self.parent().parent().parent().return_button_4.show()
        for i in reversed(range(self.vbox.count())):
            self.vbox.itemAt(i).widget().setParent(None)

        self.rated_meal_object = rated_meal()
        self.rated_meal_object.show_details_rated.hide()
        self.vbox.addWidget(self.rated_meal_object)
        for i in range(1, 10):
            self.details_rated_meal_object = details_rated_meal()
            self.vbox.addWidget(self.details_rated_meal_object)

##########################################################################
    def to_show_list_of_reservation(self):
        for i in range(1, 10):
            self.list_of_reservation_object = list_of_reservation()
            self.vbox.addWidget(self.list_of_reservation_object)

    def to_show_list_of_generated_code(self):
        for i in range(1, 10):
            self.list_of_generated_code_object = list_of_generated_code()
            self.vbox.addWidget(self.list_of_generated_code_object)

# if __name__ == "__main__":
#     import sys
#     app = QtWidgets.QApplication(sys.argv)
#     new = scroll_area()
#     sys.exit(app.exec_())
