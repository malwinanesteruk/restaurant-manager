from PyQt5 import QtCore, QtGui, QtWidgets, uic
from PyQt5.QtCore import *
from PyQt5.QtGui import *


class your_reservation(QtWidgets.QWidget):
    def __init__(self):
        super(your_reservation, self).__init__()  # Call the inherited classes __init__ method
        uic.loadUi("your_reservation.ui", self)  # Load the .ui file

        self.data_reservation = self.findChild(QtWidgets.QLabel, "data_reservation")

        self.data_reservation_change = self.findChild(QtWidgets.QLabel, "data_reservation_change")

        self.type_of_table = self.findChild(QtWidgets.QLabel, "type_of_table")

        self.type_of_table_change = self.findChild(QtWidgets.QLabel, "type_of_table_change")

        self.table_number = self.findChild(QtWidgets.QLabel, "table_number")

        self.table_number_change = self.findChild(QtWidgets.QLabel, "table_number_change")

        self.communique_cancel_reservation = self.findChild(QtWidgets.QLabel, "communique_cancel_reservation")
        self.communique_cancel_reservation.hide()

        self.your_reservation_background = self.findChild(QtWidgets.QLabel, "your_reservation_background")
        self.your_reservation_background.setStyleSheet("background-color: lightgreen")

        self.cancel_reservation = self.findChild(QtWidgets.QPushButton, "cancel_reservation")
        self.cancel_reservation.clicked.connect(self.to_cancel_reservation)

    def to_cancel_reservation(self):
        self.cancel_reservation.hide()
        self.communique_cancel_reservation.show()

#         self.show()
#
# if __name__ == "__main__":
#     import sys
#     app = QtWidgets.QApplication(sys.argv)
#     new = your_reservation()
#     sys.exit(app.exec_())