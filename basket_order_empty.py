from PyQt5 import QtCore, QtGui, QtWidgets, uic

class basket_order_empty(QtWidgets.QWidget):
    def __init__(self):
        super(basket_order_empty, self).__init__()  # Call the inherited classes __init__ method
        uic.loadUi("basket_order_empty.ui", self)  # Load the .ui file

        self.communique_label = self.findChild(QtWidgets.QLabel, "communique_label")

        self.plus_button = self.findChild(QtWidgets.QPushButton, "plus_button")

#         self.show()
#
# if __name__ == "__main__":
#     import sys
#     app = QtWidgets.QApplication(sys.argv)
#     new = basket_order_empty()
#     sys.exit(app.exec_())