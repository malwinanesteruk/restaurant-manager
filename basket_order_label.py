from PyQt5 import QtCore, QtGui, QtWidgets, uic
from PyQt5.QtCore import *
from PyQt5.QtGui import *

class basket_order_label(QtWidgets.QWidget):
    def __init__(self):
        super(basket_order_label, self).__init__()  # Call the inherited classes __init__ method
        uic.loadUi("basket_order_label.ui", self)  # Load the .ui file

        self.order_label = self.findChild(QtWidgets.QLabel, "order_label")
        self.order_label.setFont(QFont('Arial Black', 20, weight=QFont.Bold))

        # self.show()
#
# if __name__ == "__main__":
#     import sys
#     app = QtWidgets.QApplication(sys.argv)
#     new = basket_order_label()
#     sys.exit(app.exec_())