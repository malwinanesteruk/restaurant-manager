from PyQt5 import QtCore, QtGui, QtWidgets, uic

from PyQt5.QtCore import *
from PyQt5.QtGui import *

class cash_register(QtWidgets.QWidget):
    def __init__(self):
        super(cash_register, self).__init__()  # Call the inherited classes __init__ method
        uic.loadUi("cash_register.ui", self)  # Load the .ui file

        self.background_cash_register = self.findChild(QtWidgets.QLabel, "background_cash_register")
        self.background_cash_register.setStyleSheet("background-color: lightgreen")

        self.edit_data_label = self.findChild(QtWidgets.QLabel, "edit_data_label")
        self.edit_data_label.setFont(QFont('Arial', 24, weight=QFont.Bold))

        self.return_button_3 = self.findChild(QtWidgets.QPushButton, "return_button_3")
        self.return_button_3.clicked.connect(self.return_to_order)

        self.delivery_data_label = self.findChild(QtWidgets.QLabel, "delivery_data_label")
        self.delivery_data_label.setFont(QFont('Arial', 16, weight=QFont.Bold))

        self.name_line = self.findChild(QtWidgets.QLineEdit, "name_line")

        self.name_label = self.findChild(QtWidgets.QLabel, "name_label")

        self.surname_line = self.findChild(QtWidgets.QLineEdit, "surname_line")

        self.surname_label = self.findChild(QtWidgets.QLabel, "surname_label")

        self.street_line = self.findChild(QtWidgets.QLineEdit, "street_line")

        self.street_label = self.findChild(QtWidgets.QLabel, "street_label")

        self.street_number_line = self.findChild(QtWidgets.QLineEdit, "street_number_line")
        self.street_number_line.setMaxLength(10)

        self.street_number_label = self.findChild(QtWidgets.QLabel, "street_number_label")

        self.apartment_number_line = self.findChild(QtWidgets.QLineEdit, "apartment_number_line")
        self.apartment_number_line.setMaxLength(4)

        self.apartment_number_label = self.findChild(QtWidgets.QLabel, "apartment_number_label")

        self.payment_label = self.findChild(QtWidgets.QLabel, "payment_label")
        self.payment_label.setFont(QFont('Arial', 16, weight=QFont.Bold))

        self.cash_button = self.findChild(QtWidgets.QPushButton, "cash_button")
        self.cash_button.clicked.connect(self.open_end_order_cash)

        self.internet_button = self.findChild(QtWidgets.QPushButton, "internet_button")
        self.internet_button.clicked.connect(self.open_end_order_internet)

        self.to_pay_label = self.findChild(QtWidgets.QLabel, "to_pay_label")

    def terms_of_payment(self):
        if self.name_line.text() == "" or self.surname_line.text() == "" or self.street_line.text() == "" or self.street_number_line.text() == "":
            self.error_message_label.setText("Uzupełnij brakujące dane.")
            self.error_message_label.show()
            return False
        else:
            return True

    def return_to_order(self):
        self.return_button_3.hide()
        self.parent().parent().parent().to_return_to_order()

    def open_end_order_cash(self):
        if self.terms_of_payment() == True:
            self.parent().parent().parent().show_end_order_cash()

    def open_end_order_internet(self):
        if self.terms_of_payment() == True:
            self.parent().parent().parent().show_end_order_internet()



#         self.show()
#
# if __name__ == "__main__":
#     import sys
#     app = QtWidgets.QApplication(sys.argv)
#     new = cash_register()
#     sys.exit(app.exec_())