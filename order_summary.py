from PyQt5 import QtCore, QtGui, QtWidgets, uic
from PyQt5.QtCore import *
from PyQt5.QtGui import *

class order_summary(QtWidgets.QWidget):
    def __init__(self):
        super(order_summary, self).__init__()  # Call the inherited classes __init__ method
        uic.loadUi("order_summary.ui", self)  # Load the .ui file

        self.order_price = self.findChild(QtWidgets.QLabel, "order_price")

        self.order_price_change = self.findChild(QtWidgets.QLabel, "order_price_change")

        self.delivery_price = self.findChild(QtWidgets.QLabel, "delivery_price")

        self.delivery_price_change = self.findChild(QtWidgets.QLabel, "delivery_price_change")

        self.discount_code = self.findChild(QtWidgets.QLabel, "discount_code")

        self.discount_code_change = self.findChild(QtWidgets.QLabel, "discount_code_change")

        self.total_price = self.findChild(QtWidgets.QLabel, "total_price")

        self.total_price_change = self.findChild(QtWidgets.QLabel, "total_price_change")

        self.pay_button = self.findChild(QtWidgets.QPushButton, "pay_button")
        self.pay_button.setFont(QFont('Arial', 10, weight=QFont.Bold))
        self.pay_button.clicked.connect(self.show_cash_register)

        self.background_order_summary = self.findChild(QtWidgets.QLabel, "background_order_summary")
        self.background_order_summary.setStyleSheet("background-color: lightgreen")

    def show_cash_register(self):
        self.parent().parent().parent().parent().parent().to_open_cash_register()
#         self.show()
#
# if __name__ == "__main__":
#     import sys
#     app = QtWidgets.QApplication(sys.argv)
#     new = order_summary()
#     sys.exit(app.exec_())