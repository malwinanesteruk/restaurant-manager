from PyQt5 import QtCore, QtGui, QtWidgets, uic

class discount_coupon(QtWidgets.QWidget):
    def __init__(self):
        super(discount_coupon, self).__init__()  # Call the inherited classes __init__ method
        uic.loadUi("discount_coupon.ui", self)  # Load the .ui file

        self.name_coupon = self.findChild(QtWidgets.QLabel, "name_coupon")

        self.name_coupon_change = self.findChild(QtWidgets.QLabel, "name_coupon_change")

        self.coupon_text = self.findChild(QtWidgets.QLabel, "coupon_text")

        self.coupon_text_change = self.findChild(QtWidgets.QLabel, "coupon_text_change")

        self.status = self.findChild(QtWidgets.QLabel, "status")

        self.status_change = self.findChild(QtWidgets.QLabel, "status_change")

        self.add_coupon_button = self.findChild(QtWidgets.QPushButton, "add_coupon_button")

        self.background_discount_coupon = self.findChild(QtWidgets.QLabel, "background_discount_coupon")
        self.background_discount_coupon.setStyleSheet("background-color: lightgreen")
#
#         self.show()
#
# if __name__ == "__main__":
#     import sys
#     app = QtWidgets.QApplication(sys.argv)
#     new = discount_coupon()
#     sys.exit(app.exec_())