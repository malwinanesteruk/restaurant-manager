from PyQt5 import QtCore, QtGui, QtWidgets, uic

from PyQt5.QtCore import *
from PyQt5.QtGui import *

class menu_meal(QtWidgets.QWidget):
    def __init__(self):
        super(menu_meal, self).__init__()  # Call the inherited classes __init__ method
        uic.loadUi("menu_meal.ui", self)  # Load the .ui file

        self.name_meal = self.findChild(QtWidgets.QLabel, "name_meal")
        self.name_meal.setFont(QFont('Arial', 10, weight=QFont.Bold))

        self.price_meal = self.findChild(QtWidgets.QLabel, "price_meal")

        self.plus_button = self.findChild(QtWidgets.QPushButton, "plus_button")
        self.plus_button.clicked.connect(self.click)

        self.components = self.findChild(QtWidgets.QLabel, "components")

        self.background_menu_meal = self.findChild(QtWidgets.QLabel, "background_menu_meal")
        self.background_menu_meal.setStyleSheet("background-color: lightgreen")

    def click(self):
        self.parent().parent().parent().parent().parent().parent().parent().new_meal_in_left_scrollbar(self.name_meal.text(), self.price_meal.text())

#         self.show()
#
# if __name__ == "__main__":
#     import sys
#     app = QtWidgets.QApplication(sys.argv)
#     new = menu_meal()
#     sys.exit(app.exec_())