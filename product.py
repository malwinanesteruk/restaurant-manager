from PyQt5 import QtCore, QtGui, QtWidgets, uic

class product(QtWidgets.QWidget):
    def __init__(self):
        super(product, self).__init__()  # Call the inherited classes __init__ method
        uic.loadUi("product.ui", self)  # Load the .ui file

        self.name_product = self.findChild(QtWidgets.QLabel, "name_product")

        self.quantity = self.findChild(QtWidgets.QLabel, "quantity")

        self.price = self.findChild(QtWidgets.QLabel, "price")

        self.background_product = self.findChild(QtWidgets.QLabel, "background_product")
        self.background_product.setStyleSheet("background-color: lightgreen")

#         self.show()
#
# if __name__ == "__main__":
#     import sys
#     app = QtWidgets.QApplication(sys.argv)
#     new = product()
#     sys.exit(app.exec_())