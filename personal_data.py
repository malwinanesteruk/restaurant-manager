from PyQt5 import QtCore, QtGui, QtWidgets, uic

class personal_data(QtWidgets.QWidget):
    def __init__(self):
        super(personal_data, self).__init__()  # Call the inherited classes __init__ method
        uic.loadUi("personal_data.ui", self)  # Load the .ui file

        self.close_personal_data = self.findChild(QtWidgets.QPushButton, "close_personal_data")
        self.close_personal_data.setIcon(QtGui.QIcon('close_icon.png'))
        self.close_personal_data.setIconSize(QtCore.QSize(30, 30))
        # self.close_personal_data.clicked.connect(self.close_to_personal_data)

        self.personal_data_label = self.findChild(QtWidgets.QLabel, "personal_data_label")

        self.error_message_label = self.findChild(QtWidgets.QLabel, "error_message_label")

        self.background_pd = self.findChild(QtWidgets.QLabel, "background_pd")
        self.background_pd.setStyleSheet("background-color: lightgreen")

        self.login = self.findChild(QtWidgets.QLabel, "login")
        #self.login.setText("Kurza_stopa")

        self.login_label = self.findChild(QtWidgets.QLabel, "login_label")

        self.name = self.findChild(QtWidgets.QLabel, "name")
        #self.name.setText("Maria")

        self.name_label = self.findChild(QtWidgets.QLabel, "name_label")

        self.surname = self.findChild(QtWidgets.QLabel, "surname")
        #self.surname.setText("Nowak")

        self.surname_label = self.findChild(QtWidgets.QLabel, "surname_label")

        self.adress = self.findChild(QtWidgets.QLabel, "adress")
        #self.adress.setText("Kopernika 4/32")

        self.adress_label = self.findChild(QtWidgets.QLabel, "adress_label")

        self.edit_personal_data = self.findChild(QtWidgets.QPushButton, "edit_personal_data")

        self.edit_password = self.findChild(QtWidgets.QPushButton, "edit_password")

        # self.show()

    def close_to_personal_data(self):
        self.close_personal_data.hide()
        self.personal_data_label.hide()
        self.background_pd.hide()
        self.login.hide()
        self.login_label.hide()
        self.name.hide()
        self.name_label.hide()
        self.surname.hide()
        self.surname_label.hide()
        self.adress.hide()
        self.adress_label.hide()
        self.edit_personal_data.hide()
        self.edit_password.hide()
        self.error_message_label.hide()
#
# if __name__ == "__main__":
#     import sys
#     app = QtWidgets.QApplication(sys.argv)
#     new = personal_data()
#     sys.exit(app.exec_())